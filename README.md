# data_dictionary

#### 根据MySql注释生成对应的数据字典生结构，并返回 html、Markdown、json格式

````php

$db  = [
	// [required]
	'type'      => 'mysql',
	'host'      => '127.0.0.1',
	'database'  => 'test',
	'username'  => 'root',
	'password'  => 'root',
	// [optional]
	'charset'   => 'utf8mb4',
	'collation' => 'utf8mb4_general_ci',
	'port'      => 3306,
	// [optional] The table prefix. All table names will be prefixed as PREFIX_table.
	'prefix'    => 'doc_',
];
$doc = new Dictionary();
echo $doc->make($db,'html');
````

![输入图片说明](1655434305842.jpg)